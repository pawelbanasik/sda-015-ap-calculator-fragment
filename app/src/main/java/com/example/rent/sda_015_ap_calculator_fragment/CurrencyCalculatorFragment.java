package com.example.rent.sda_015_ap_calculator_fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by RENT on 2017-04-25.
 */

public class CurrencyCalculatorFragment extends Fragment {

    private EditText editTextOne;
    private Button buttonOne;
    private TextView textViewOne;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_currency_calculator, container, false);

        textViewOne = (TextView) view.findViewById(R.id.text_view_one);
//        buttonOne = (Button) view.findViewById(R.id.button_one);
        editTextOne = (EditText) view.findViewById(R.id.edit_text_one);

        view.findViewById(R.id.button_one).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    String plnString = editTextOne.getText().toString();
                    double pln = Double.parseDouble(plnString);
                    double usd = pln * 3.8898;
                    String usdString = usd + " USD";
                    textViewOne.setText(usdString);
                } catch (NumberFormatException e) {
                    e.printStackTrace();

                    Context context = getActivity();
                    CharSequence text = "Podaj liczbe.";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }


            }
        });


        return view;


    }
}
