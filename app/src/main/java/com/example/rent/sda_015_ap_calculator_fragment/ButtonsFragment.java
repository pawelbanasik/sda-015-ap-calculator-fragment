package com.example.rent.sda_015_ap_calculator_fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

/**
 * Created by RENT on 2017-04-25.
 */

public class ButtonsFragment extends Fragment {


    // mozna tez tak pisac mniej
    // nie musze zmiennej zapamietywac zeby cos robic



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_buttons, container, false);

        final MainActivity mainActivity = (MainActivity) getActivity();

        view.findViewById(R.id.investmentButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showInvestmentForm();
            }
        });

        view.findViewById(R.id.currencyCalculatorButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showCurrencyCalculator();

            }
        });


        // przyklad animacji
        final View button = view.findViewById(R.id.currencyCalculatorButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.showCurrencyCalculator();
                Animation animation = AnimationUtils.loadAnimation(getActivity(), R.anim.animation_example);
                button.startAnimation(animation);
            }
        });

        return view;
    }
}
